package org.pan.bean;

import lombok.Data;

import javax.persistence.Entity;

@Entity
@Data
public class LoginUser extends BasicDomain {

    private String username;
    private String password;
}
