package org.pan.bean;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Setter
@Getter
@MappedSuperclass
public class BasicDomain {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private LocalDateTime createTime;
    private LocalDateTime updateTime;

    @PreUpdate
    public void PreUpdate() {
        this.updateTime = LocalDateTime.now();
    }

    @PrePersist
    public void PrePersist() {
        this.createTime = LocalDateTime.now();
    }
}
