package org.pan.bean;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
@Getter
@Setter
public class UserGroupInfo extends BasicDomain {

    private String groupName;
    @ManyToOne
    private UserGroupInfo parent;
    @OneToMany
    private List<UserGroupInfo> children;
}
