package org.pan.bean;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Entity
@Setter
@Getter
public class UserInfo extends BasicDomain {
    private String userId;
    private String userName;
    private String userCardId;
    private UserInfoStatusEnum userStatus = UserInfoStatusEnum.正常;
}
