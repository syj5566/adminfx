package org.pan;

import com.google.common.eventbus.EventBus;
import com.google.inject.persist.PersistService;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import lombok.extern.slf4j.Slf4j;
import org.pan.controller.LoginController;
import org.pan.service.SystemService;

@Slf4j
public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        log.info("系统开始启动");

        primaryStage.setTitle("门禁管理平台");
        primaryStage.setScene(new Scene(FXMLLoader.load(getClass().getClassLoader().getResource("fxml/loading.fxml"))));
        primaryStage.getIcons().add(new Image("/img/firewall_on_security_72px_43150_easyicon.net.png"));
        primaryStage.initStyle(StageStyle.UNDECORATED);
        primaryStage.setResizable(true);
        primaryStage.show();

        log.info("系统预加载界面己启动");

        GuiceInit(primaryStage);

        log.info("系统启动完成");
    }

    private void GuiceInit(Stage primaryStage) {
        GuiceFx.init(primaryStage);
        new Thread(() -> {
            log.info("开始启动数据服务");
            PersistService persistService = GuiceFx.injector.getInstance(PersistService.class);
            persistService.start();
            log.info("启动数据服务完成");

            log.info("开始检查默认数据");
            SystemService systemService = GuiceFx.injector.getInstance(SystemService.class);
            systemService.init();
            log.info("检查默认数据完成");

            Platform.runLater(()-> primaryStage.setScene(new Scene(GuiceFx.load(LoginController.class))));

        }).start();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
