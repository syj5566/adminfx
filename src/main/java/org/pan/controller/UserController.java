package org.pan.controller;

import javafx.fxml.Initializable;
import javafx.scene.control.Pagination;
import org.pan.Controller;

import java.net.URL;
import java.util.ResourceBundle;

@Controller(fxml = "/fxml/user.fxml")
public class UserController implements Initializable {
    public Pagination page;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
