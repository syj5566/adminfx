package org.pan.controller;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import org.pan.Controller;
import org.pan.GuiceFx;

import java.net.URL;
import java.util.ResourceBundle;

@Controller(fxml = "/fxml/device/device.fxml")
public class DeviceController implements Initializable {
    public BorderPane borderPane;
    private Parent deviceDetail;
    private Parent deviceGroupDetail;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        deviceDetail = GuiceFx.load(DeviceDetailController.class);
        deviceGroupDetail = GuiceFx.load(DeviceGroupDetailController.class);
    }

    public void addGroup(ActionEvent actionEvent) {
        borderPane.setCenter(deviceGroupDetail);
    }

    public void add(ActionEvent actionEvent) {
        borderPane.setCenter(deviceDetail);
    }
}
