package org.pan.controller;

import com.google.inject.Inject;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import org.pan.Controller;
import org.pan.Dialogs;
import org.pan.GuiceFx;

import java.net.URL;
import java.util.ResourceBundle;

@Controller(fxml = "/fxml/layout.fxml")
public class LayoutController implements Initializable {

    public ToggleButton cardUserManage;
    public ScrollPane content;
    public HBox top;
    public ToggleButton deviceManage;
    public ToggleButton loginUserManage;
    public ToggleButton logManage;
    public ToggleButton setManage;
    public ToggleButton privilegeManage;
    public ToggleButton monitorManage;

    private double xOffset = 0;
    private double yOffset = 0;
    @Inject
    private Stage stage;
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        top.setOnMousePressed(event -> {
            xOffset = event.getSceneX();
            yOffset = event.getSceneY();
        });

        top.setOnMouseDragged(event -> {
            stage.setX(event.getScreenX() - xOffset);
            stage.setY(event.getScreenY() - yOffset);
        });

        cardUserManage.setOnMouseClicked(event -> content.setContent(GuiceFx.load(UserController.class)));

        deviceManage.setOnMouseClicked(event -> content.setContent(GuiceFx.load(DeviceController.class)));

        loginUserManage.setOnMouseClicked(event -> content.setContent(GuiceFx.load(LoginUserController.class)));

        logManage.setOnMouseClicked(event -> content.setContent(GuiceFx.load(LogController.class)));

        setManage.setOnMouseClicked(event -> content.setContent(GuiceFx.load(SetController.class)));

        privilegeManage.setOnMouseClicked(event -> content.setContent(GuiceFx.load(PrivilegeController.class)));

        monitorManage.setOnMouseClicked(event -> content.setContent(GuiceFx.load(MonitorController.class)));
    }

    public void logout(MouseEvent mouseEvent) {
        Boolean bool = Dialogs.confirm("提示", "确认要注销当前用户吗");
        if(bool){
            System.exit(1);
        }
    }

    public void exit(MouseEvent mouseEvent) {
        Boolean bool = Dialogs.confirm("提示", "确认要注销当前用户吗");
        if(bool){
            System.exit(1);
        }
    }

    public void full(MouseEvent mouseEvent) {
        stage.setFullScreen(!stage.isFullScreen());
    }
}
