# adminfx

#### 介绍
javafx 管理模板

#### 软件架构
javafx + maven + guice

#### 开发环境
win10 + idea + java8

#### 安装教程

1. git clone https://gitee.com/panmingzhi/adminfx.git
2. mvn package
3. 双击运行target目录下的 adminfx-1.0-SNAPSHOT.jar

#### 截图
启动图

![输入图片说明](https://gitee.com/uploads/images/2019/0503/233838_357b61d9_87848.png "微信图片_20190503233823.png")

登录

![输入图片说明](https://gitee.com/uploads/images/2019/0503/215350_e8826e7c_87848.png "微信图片_20190503214850.png")

主界面

![输入图片说明](https://gitee.com/uploads/images/2019/0503/215407_2d639748_87848.png "微信图片_20190503214905.png")

设备管理

![输入图片说明](https://gitee.com/uploads/images/2019/0503/215418_4b3748a0_87848.png "微信图片_20190503214912.png")

登录用户管理

![输入图片说明](https://gitee.com/uploads/images/2019/0503/215452_1b33b3dc_87848.png "微信图片_20190503214916.png")

操作日志

![输入图片说明](https://gitee.com/uploads/images/2019/0503/215538_7e3c6348_87848.png "微信图片_20190503214923.png")

卡片用户管理

![输入图片说明](https://gitee.com/uploads/images/2019/0503/215632_872980e3_87848.png "微信图片_20190503214927.png")

授权管理

![输入图片说明](https://gitee.com/uploads/images/2019/0503/215611_aa147ced_87848.png "微信图片_20190503214932.png")

用户退出提示

![输入图片说明](https://gitee.com/uploads/images/2019/0503/215647_5129216f_87848.png "微信图片_20190503214936.png")

